package gopherjselectron

type EventEmitter interface {
	On(Event string, listener func(args ...interface{}))
}
