# gopherjs-electron

https://bitbucket.org/gedw99/md-desktop-x


## What is this ?
Its a way of using Electron (https://github.com/atom/electron/) to build desktop apps using only golang.

It makes heavy use of https://github.com/gopherjs/gopherjs.


## Supported systems
- Windows 10
- OSX

## Install
- Go 1.7
	- https://golang.org/dl/
	- Download, run installer & set your environment variables.
	- Check by running:  ``` go env ```
- NodeJS
	- https://nodejs.org/en/download/
	- You can use the LTS or Current version
	- Download, run installer.
	- Check by running:
		- ``` npm -version ```
		- ``` node --version ```
- Glide is used for dependency management / vendoring.
	- https://github.com/Masterminds/glide
	- To install, just run: ``` curl https://glide.sh/get | sh ```
- Gopherjs
	- https://github.com/gopherjs/gopherjs
	- It does not work with vendoring.
	- To install, run : ``` go get -u github.com/gopherjs/gopherjs ```
- Electron (prebuilt)
	- https://github.com/electron-userland/electron-prebuilt
	- To install, run : ``` npm install -g electron-prebuilt ```


## Usage
To build and run it.

````
./t.buildrun.sh
````


## Roadmap
- DONE: Build & starts FrontEnd (Electron) and Backend (golang).
- DONE: Basic communication between Backend and Frontend working.

- Change Electron Menu
	- No Menu is best, OSX does not allow different screens to have different menus.  

- How can the Render process control the Modals etc ?
	- Need to use 
- Pack the FrontEnd into the golang binary, so there are no web files on the hard disk.
	- https://github.com/shurcooL/vfsgen

- Change for all OS's
	- icons
	- taskbar
	- notifications
	- exe (windows), mdg (osx), ? (liux)
- Build: Shrink the size of the web code.
	- https://github.com/tdewolff/minify
- Build: Shrink the size of the gopherjs code
	- can do a simple fidn replace on package names, like an obfuscator does.

- More gopherjs modules: Polymer, etc, etc
- Add BoltDB for a Desktop and Mobile DB
- Add Cordova Packaging so it runs on mobile.
- Write golang based scripts to package everything, replacing the existing .sh & .bat files.



## Test

For tests used Jasmine (http://jasmine.github.io/) and adapter fo go https://github.com/arvitaly/gopherjs-jasmine

	go get github.com/arvitaly/gopherjs-jasmine
	npm install electron-prebuilt -g
	npm install
	npm test

## Docker

Also you can use docker-image for electron (includes NodeJS, Golang, gopherjs, electron-prebuilt). Example for run app in docker-container in file .drone.yml (config for drone.io CI)

	docker pull arvitaly/electron-go
