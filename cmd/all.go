package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"syscall"

	b "bitbucket.org/gedw99/md-desktop-x/backend"
)

// configs
var backendHTTPPath = "127.0.0.1:8080"
var frontendFSPath = "../frontend/."

func main() {

	flag.Parse()

	// start backend

	http.HandleFunc("/", b.HomeHandler)
	http.HandleFunc("/hello/", b.HelloHandler)
	http.HandleFunc("/api/test/", b.ApiTestHandler)

	printOutput([]byte(fmt.Sprintf("backend listening on: %s", backendHTTPPath)))
	go func() {
		http.ListenAndServe(backendHTTPPath, nil)
	}()

	// start frontend
	printOutput([]byte(fmt.Sprintf("frontend starting on: %s", frontendFSPath)))

	cmd := exec.Command("electron", frontendFSPath)
	printCommand(cmd)

	var waitStatus syscall.WaitStatus
	if err := cmd.Run(); err != nil {
		printError(err)
		// Did the command fail because of an unsuccessful exit code
		if exitError, ok := err.(*exec.ExitError); ok {
			waitStatus = exitError.Sys().(syscall.WaitStatus)
			printOutput([]byte(fmt.Sprintf("%d", waitStatus.ExitStatus())))
		}
	} else {
		// Command was successful
		waitStatus = cmd.ProcessState.Sys().(syscall.WaitStatus)
		printOutput([]byte(fmt.Sprintf("%d", waitStatus.ExitStatus())))
	}

}

func printCommand(cmd *exec.Cmd) {
	fmt.Printf("==> Executing: %s\n", strings.Join(cmd.Args, " "))
}

func printError(err error) {
	if err != nil {
		os.Stderr.WriteString(fmt.Sprintf("==> Error: %s\n", err.Error()))
	}
}

func printOutput(outs []byte) {
	if len(outs) > 0 {
		fmt.Printf("==> Output: %s\n", string(outs))
	}
}
