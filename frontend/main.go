// +build js

package main

import (
	"flag"
	"fmt"

	"github.com/gopherjs/gopherjs/js"

	electron "bitbucket.org/gedw99/md-desktop-x/gopherjselectron"
)

func main() {

	flag.Parse()

	fmt.Println("starting frontend... ")
	var app = electron.GetApp()
	fmt.Println("app path: " + app.GetAppPath())

	app.OnWillQuit(func(event *js.Object) {
		fmt.Println("app OnWillQuit fired: ")
		
	})

	// once ready we can do stuff..
	app.OnReady(func() {

		// Window 0 is the main window
		wOptions := map[string]interface{}{
				"title": "md-desktop-x",
				"fullscreen": false,
				"x": 1,
				"y": 1,
			}
		var w = electron.NewBrowserWindow(&wOptions)

		// Menu
		mOptions := map[string]interface{}{
				"setApplicationMenu": "null",
			}
		w.SetMenu(&mOptions)
		
		//fmt.Println("w window title: " + w.GetTitle())

		//w.LoadURL("file:///"+js.Global.Get("process").Call("cwd").String()+"index.html", nil)

		homePath := "file:///"+app.GetAppPath()+"/assets/index.html"
		w.LoadURL(homePath, nil)
		//w.LoadURL("http://127.0.0.1:8080/", nil)

		// wire events
		w.GetWebContents().OnDidStopLoading(func() {
			fmt.Println("w OnDidStopLoading. Title: " + w.GetTitle(), )
		})

		
		w.OnClose(func(event *js.Object) {
			fmt.Println("w OnClose fired.")
		})
		

		w.OnClosed(func() {
			fmt.Println("w OnClosed fired.")
			w.Destroy()

		})

		w.GetWebContents().OnWillNavigate(func(event *js.Object, url string) {
			fmt.Println("w OnWillNavigate fired.")
			//w.Close()
		})



		// W1 is a Modal window
		w1Options := map[string]interface{}{
				"title": "md-desktop-x-modal",
				"parent": "top",
				"modal": true,
				"frame": true,
				"show": true,
			}
		var w1 = electron.NewBrowserWindow(&w1Options)

		w1.LoadURL("file:///"+app.GetAppPath()+"/assets/index.1.html", nil)

		w1.GetWebContents().OnDidStopLoading(func() {
			fmt.Println("w1 OnDidStopLoading. Title: " + w1.GetTitle())
			
			
		})

		w1.OnClosed(func() {
			fmt.Println("w1 OnClosed.")
			w1.Destroy()
		})

	})

}

/*
var _ = jasmine.Run(func() {
	jasmine.Describe("App", func() {
		var app = electron.GetApp()
		jasmine.ItAsync("OnReady", func(done func()) {
			app.OnReady(func() {
				done()
			})
		})
		jasmine.It("GetAppPath", func() {
			jasmine.Expect(app.GetAppPath() != "").ToBeTruthy()
		})
		jasmine.ItAsync("OnWillQuit", func(done func()) {
			var firstTime = true
			app.OnWillQuit(func(event *js.Object) {
				if firstTime {
					event.Call("preventDefault")
				}
				firstTime = false
				done()
			})
			var br = electron.NewBrowserWindow(nil)
			br.Close()
		})
	})
})
*/
